#Josh Churchill
#Project 9
from random import randint 
print("Please pick a file you'd like to be encrypted")
Josh = pickAFile()
tFile = open(Josh, 'r')
charFile = list(tFile.read())
tFile.close()
print("Please pick a location to save your encrypted file to")
encLocation = pickAFolder()
finLocation = encLocation + "encryptedFile.txt"
programFile = open(finLocation, 'a')
encryptList = []
characters = {'A' : 0, 'a' : 0, 'B' : 0, 'b' : 0, 'C' : 0, 'c' : 0, 'D' : 0, 
'd' : 0, 'E' : 0, 'e' : 0, 'F' : 0, 'f' : 0, 'G' : 0, 'g' : 0, 'H' : 0, 'h' : 0, 
'I' : 0, 'i' :0, 'J' : 0, 'j' : 0, 'K' : 0, 'k' : 0, 'L' : 0, 'l' : 0, 'M' : 0, 
'm' : 0, 'N' : 0, 'n' : 0, 'O' : 0, 'o' : 0, 'P' : 0, 'p' : 0, 'Q' : 0, 'q' : 0, 
'R' : 0, 'r' :0, 'S' : 0, 's' : 0, 'T' : 0, 't' : 0, 'U' : 0, 'u' : 0, 'V' : 0, 
'v' : 0, 'X' : 0, 'x' : 0, 'Y' : 0, 'y' : 0, 'Z' : 0, 'z' : 0, '1' : 0, '2' : 0, 
'3' : 0, '4' :0, '5' : 0, '6' : 0, '7' : 0, '8' : 0, '9' : 0, '0' : 0, ' ' : 0}
usedVals = {}
i = 0
encString = ""
for key in characters:
  charValue = randint(0,100000000)
  while(charValue in usedVals):
    charValue = randint(0,100000000)
  fullVar = str(charValue) + "-"
  characters[key] = str(charValue) 
  usedVals[charValue] = i
  i += 1
  encryptList.append(fullVar)
#print(encryptList)
for char in charFile:
  if(char in characters):
    encString += str(characters[char])
    encString += "-"
c = 0
programFile.write("Encrypted Text: \n")

programFile.write(str(encString+ "\n"))
global decryptString
decryptString = ""
encryptText = encString.split("-")
def decrypt():
  decryptString = ""
  for char in encryptText:
    for key in characters:
      if(char == characters[key]):
        decryptString += key
  
  #print(decryptString)
  programFile.write("Decrypted Text: \n")
  programFile.write(decryptString + "\n")
  programFile.close()

      

userInput = raw_input("Your text is now encrypted. Would you like to decrypt your text now? ")
if(userInput == "y" or userInput == "Y" or userInput == "yes" or userInput == "Yes"):
  decrypt()
else:
  print("Thanks for using CB technologies!")



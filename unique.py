#Josh Churchill
#Project 9
print("Please pick the first file you'd like to be analyzed")
file = pickAFile()
rFile = open(file, 'r')
fileAnalysis = rFile.read()
fileList = fileAnalysis.split()
fileSet = set()
for char in fileList:
  fileSet.add(char)

print("Here are all the unique words in your file")
print(fileSet)
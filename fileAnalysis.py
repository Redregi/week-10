#Josh Churchill
#Project 9
print("Please pick the first file you'd like to be analyzed")
chooseOne = pickAFile()
fileOne = open(chooseOne, 'r')
oneAnalysis = fileOne.read()
print("Please pick the second file you'd liket to be analyzed")
chooseTwo = pickAFile()
fileTwo = open(chooseTwo, 'r')
twoAnalysis = fileTwo.read()
oneList = oneAnalysis.split()
twoList = twoAnalysis.split()

uniqueChar = set()
numberFour = set()


uniqueChar.update(oneList)
uniqueChar.update(twoList)
print("These are the unique words in both files")
print(uniqueChar)
print("")
uniqueChar.clear()

for a in oneList:
  for b in twoList:
    if(a == b):
      uniqueChar.add(a)
print("These are the words that appear in both files")
print(uniqueChar)
print("")
uniqueChar.clear()

x = 0
oneListSet  = set(oneList)
twoListSet = set(twoList)
for word in oneListSet:
  if word in twoListSet:
    x += 1 
  else:
    uniqueChar.add(word)
    numberFour.add(word)
    
print("These words appear in the first file, but not in the second ")
print(uniqueChar)
print("")
uniqueChar.clear()
for word in twoListSet:
  if word in oneListSet:
    x += 1 
  else:
    uniqueChar.add(word)
    numberFour.add(word)
    
print("These words appear in the second file, but not in the first")
print(uniqueChar)
print("")

print("These words appear in either the first or second file, but not both")
print(numberFour)



